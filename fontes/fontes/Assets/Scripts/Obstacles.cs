﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour {

    public int NumberOfObstaclesPerTile = 3;
    public List<GameObject> ObstacleObject;
    public GameObject ObstacleObject2;

    public void DestroyAllObstacles()
    {
        foreach (Transform obstacle in transform)
        {
            if(obstacle.tag != "Sideborder")
            {
                Destroy(obstacle.gameObject);
            }
        }
    }

    public void InstantiateObstacles()
    {
        for (int i = 0; i < NumberOfObstaclesPerTile; i++)
        {
            
            GameObject obstacle = Instantiate(ObstacleObject2) as GameObject;
            obstacle.transform.parent = this.transform;
            setPosition(obstacle);
        }
    }
   
    private void setPosition(GameObject obstacle)
    {
        float x = Random.Range(-0.75f, 0.75f);
        float y = 0.6f;
        float z = Random.Range(-4.2f, 4.2f);
        obstacle.transform.localPosition = new Vector3(x, y, z);
    }
}
