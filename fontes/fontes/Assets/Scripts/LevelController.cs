﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelController : MonoBehaviour {

    public static int Level = 1;
    public static string[] Words;
    public static char victory = 'D';
    public string[] vWords;


    public GameObject ChangeScenePanel;
    public Text Palavra;

    public void SetLevel(int vLevel)
    {
        Level = vLevel; 
    }

    public void SetVictory()
    {
        victory = 'S';
    }

    public void SetLoose()
    {
        victory = 'N';
    }

    public void SetWords()
    {
        Words = vWords;
    } 

    public void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void WinLevel()
    {
        ChangeScenePanel.SetActive(true);
        Level++;
        //Palavra.text = Words[Level - 1];
        ChangeScene("Stairs");
    }
}
