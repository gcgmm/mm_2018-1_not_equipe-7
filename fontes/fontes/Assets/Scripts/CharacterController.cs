﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public int lives = 3;
    public GameObject SpineBone;
    public GameObject Heart_1;
    public GameObject Heart_2;
    public GameObject Heart_3;

    public GameObject Model;
    public float AmountOfJump = 300f;
    private GameObject[] grounds;
    private GameObject[] cars;
    private bool CanJump = true;


    private void FixedUpdate()
    {
        if(SpineBone.transform.position.x > -1.8f && SpineBone.transform.position.x < 1.8f)
        {
            this.transform.position = new Vector3(SpineBone.transform.position.x
                , transform.position.y, transform.position.z);
        }
    }

    public LevelController levelController;

    public void JumpAction()
    {
        if (CanJump)
        {
            this.GetComponent<Rigidbody>().AddForce(transform.up * AmountOfJump);
            //CanJump = false;
            //StartCoroutine(JumpWait());
        }
    }

    IEnumerator JumpWait()
    {
        yield return new WaitForSeconds(1f);
        CanJump = true;
    }


       

 
    
    private IEnumerator FallToLose()
    {
        yield return new WaitForSeconds(2f);
        levelController.SetLoose();
        levelController.ChangeScene("Menu");
    }

    private IEnumerator GetHit()
    {
        SetLives();

        Color newColor = new Color(1, 0, 0);
        Color normalColor = Model.GetComponent<Renderer>().materials[1].color;

        for (int i = 0; i < 3; i++)
        {
            Model.GetComponent<Renderer>().materials[1].color = newColor;
            yield return new WaitForSeconds(.2f);
            Model.GetComponent<Renderer>().materials[1].color = normalColor;
            yield return new WaitForSeconds(.2f);
        }
    }

    private void SetLives()
    {
        lives--;
        switch (lives)
        {
            case 1:
                Heart_2.gameObject.SetActive(false);
                break;
            case 2:
                Heart_3.gameObject.SetActive(false);
                break;
            case 0:
                Heart_1.gameObject.SetActive(false);
                levelController.SetLoose();
                levelController.ChangeScene("Menu");
                break;
            default:
                break;
        }
    }


    private void StopGroundMovment()
    {
        grounds = GameObject.FindGameObjectsWithTag("Ground");
        foreach (GameObject ground in grounds)
        {
            ground.GetComponent<GroundMovment>().StopMoving();
        }
    }



    private void SetCharacterPos(Vector3 newPosition)
    {
        transform.position = Vector3.Slerp(transform.position, newPosition, .4f);
    }
}
