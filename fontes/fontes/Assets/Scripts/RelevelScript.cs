﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelevelScript : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Delay());
	}

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(10f);
        if (LevelController.Level < 8)
        {
            this.gameObject.GetComponent<LevelController>().ChangeScene("Main");
        }
        else
        {
            this.gameObject.GetComponent<LevelController>().SetVictory();
            this.gameObject.GetComponent<LevelController>().ChangeScene("Menu");
        }
    }
}
